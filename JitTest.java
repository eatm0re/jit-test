package edu.eatmore.jit;

import java.io.IOException;

public class JitTest {
	
	private int cc = 1;
	
	public static void main(String[] args) throws IOException {
		JitTest test = new JitTest();
		while (true) {
			System.in.read();
			test.testFoo();
		}
	}
	
	public void testFoo() {
		cc++;
	}
	
}
